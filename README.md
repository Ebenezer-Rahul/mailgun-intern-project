# mailgun-intern-project

## Email Tracking Using Mailgun

## Description
This Project Exposes an API which allows you to send Email using Mailgun.

So When ever an email is sent mailgun tracks events like status of the email.

### Events

1. Delivered
2. Opened
3. clicked
4. Temporary Failure
5. Permanent Failure
6. Reported Spam 
7. Unsubcribed

Events other than Opened and clicked are tracked by default by Mailgun.
Mailgun provides webhooks to track these events so anytime an event on an email occurs it posts to our specified url

So everytime an event occurs this server checks that the event is in fact from the mailgun by validating each event with it's signature and saves all the events into a mongodb database.


### Usage 

This project exposes a simple api with the following routes

#### /smtp/sendMessage

you need to post with the following JSON Schema: 
```
{
    "from" : "from@email.com",
    "to" : "somebody@somedomain.com",
    "subject" : "Subject goes Here",
    "body" : "Here goes some text"
}
```
Here as you can see you can only send plain text as the body so you can't track events like opened, clicked here.

It will send your message using SMTP protocol to the mailgun server.
```
/smtp/html/sendMessage
```

The only thing here you can write HTML in the body field of JSON.
And it enables tracking of all events

Similarly you have 
```
/mailgun-api/sendMessage
```
```
/mailgun-api/html/sendMessage 
```

Both of which use mailgun API to send the email to mailgun server

All of them provide a unique messageID as a response to a post request.

You can use this messageID to track events on that particular message

You can get all the events of a message by sending a get request at 
```
/track/message=<messageID>
```
it will send an array of all the events of that message

It also exposes an endpoint 
```
/track/failedMessages 
```
which will return an array of all the failure Events both Temporary and Permanent failures.

## Side Notes

I have an index on messageID in the database to make the ```/track/message=<messageID>```

#### On Events

Events like Delivered, Opened, Clicked, Reported-Spam, Unsubscribe are self explanatory

So Temporary and Permanent Failures are :

**Permanent Failure** occurs when the user doesn't exist or their mail server is not running.



**Temporary Failure** occurs if the user has his mailbox full, in this there are two ways temporary failure may occur
1. In first case the recipient mail server may bounce back immediately in this case mailgun tries again for some time but if this happens again and again it will just quit retrying.
2. In second case the mail server accepts the mail then it rejects it at a later time before it sends to the user. in this case mailgun doesn't attempt to send it again

