package com.sprinklr.mail.model;

public class UserVariables {
    private String myVar1;
    private String myVar2;

    public String getMyVar1() {
        return myVar1;
    }

    public void setMyVar1(String value) {
        this.myVar1 = value;
    }

    public String getMyVar2() {
        return myVar2;
    }

    public void setMyVar2(String value) {
        this.myVar2 = value;
    }

    @Override
    public String toString() {
        return "UserVariables{" +
                "myVar1='" + myVar1 + '\'' +
                ", myVar2='" + myVar2 + '\'' +
                '}';
    }
}
