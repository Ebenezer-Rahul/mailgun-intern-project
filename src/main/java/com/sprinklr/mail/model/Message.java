package com.sprinklr.mail.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.websocket.OnClose;
import org.springframework.boot.autoconfigure.kafka.KafkaProperties;

import java.util.Arrays;
import java.util.List;

public class Message {
    @JsonProperty("headers")
    private Headers headers;
    @JsonProperty("attachments")
    private List<Object> attachments;
    @JsonProperty("size")
    private long size;

    public Headers getHeaders() { return headers; }
    public void setHeaders(Headers value) { this.headers = value; }

    public List<Object> getAttachments() { return attachments; }
    public void setAttachments(List<Object> value) { this.attachments = value; }

    public long getSize() { return size; }
    public void setSize(long value) { this.size = value; }

    @Override
    public String toString() {
        return "Message{" +
                "headers=" + headers +
                ", attachments=" + attachments +
                ", size=" + size +
                '}';
    }
}
