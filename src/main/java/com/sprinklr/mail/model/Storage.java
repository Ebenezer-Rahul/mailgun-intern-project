package com.sprinklr.mail.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Storage {
    @JsonProperty("url")
    private String url;
    @JsonProperty("key")
    private String key;

    @JsonProperty("env")
    private String env;

    @JsonProperty("region")
    private String region;

    public String getURL() {
        return url;
    }

    public void setURL(String value) {
        this.url = value;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String value) {
        this.key = value;
    }

    @Override
    public String toString() {
        return "Storage{" +
                "url='" + url + '\'' +
                ", key='" + key + '\'' +
                ", env='" + env + '\'' +
                ", region='" + region + '\'' +
                '}';
    }

    public String getEnv() {
        return env;
    }

    public void setEnv(String env) {
        this.env = env;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }
}
