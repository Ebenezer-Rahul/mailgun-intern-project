package com.sprinklr.mail.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Envelope {
    @JsonProperty("transport")
    private String transport;
    @JsonProperty("sender")
    private String sender;
    @JsonProperty("sending-ip")
    private String sendingIP;
    @JsonProperty("targets")
    private String targets;

    public String getTransport() {
        return transport;
    }

    public void setTransport(String value) {
        this.transport = value;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String value) {
        this.sender = value;
    }

    public String getSendingIP() {
        return sendingIP;
    }

    public void setSendingIP(String value) {
        this.sendingIP = value;
    }

    public String getTargets() {
        return targets;
    }

    public void setTargets(String value) {
        this.targets = value;
    }

    @Override
    public String toString() {
        return "Envelope{" +
                "transport='" + transport + '\'' +
                ", sender='" + sender + '\'' +
                ", sendingIP='" + sendingIP + '\'' +
                ", targets='" + targets + '\'' +
                '}';
    }
}
