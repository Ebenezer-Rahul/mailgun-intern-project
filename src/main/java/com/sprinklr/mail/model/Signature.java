package com.sprinklr.mail.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Signature {
    @JsonProperty("token")
    private String token;
    @JsonProperty("timestamp")
    private String timestamp;
    @JsonProperty("signature")
    private String signature;

    public String getToken() {
        return token;
    }

    public void setToken(String value) {
        this.token = value;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String value) {
        this.timestamp = value;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String value) {
        this.signature = value;
    }

    @Override
    public String toString() {
        return "Signature{" +
                "token='" + token + '\'' +
                ", timestamp='" + timestamp + '\'' +
                ", signature='" + signature + '\'' +
                '}';
    }
}
