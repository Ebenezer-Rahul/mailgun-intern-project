package com.sprinklr.mail.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Event {

    @JsonProperty("signature")
    private Signature signature;

    @JsonProperty("event-data")
    private EventData eventData;

    public Signature getSignature() {
        return signature;
    }

    public void setSignature(Signature value) {
        this.signature = value;
    }

    public EventData getEventData() {
        return eventData;
    }

    public void setEventData(EventData value) {
        this.eventData = value;
    }

    @Override
    public String toString() {
        return "Event{" +
                "signature=" + signature +
                ", eventData=" + eventData +
                '}';
    }
}
