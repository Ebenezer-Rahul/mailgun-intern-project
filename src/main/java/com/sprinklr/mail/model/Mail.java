package com.sprinklr.mail.model;

public class Mail {

    public String from;

    public String to;

    public String subject;

    public String body;


    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String checkNULL() {
        String errorMessage = "";
        if (this.from == null) {
            errorMessage += "From Field Cannot be NULL\n";
        }
        if(this.to == null) {
            errorMessage += "From Field Cannot be NULL\n";
        }
        if(this.subject == null) {
            errorMessage += "From Field Cannot be NULL\n";
        }
        if(this.body == null)  {
            errorMessage +="Body Field cannot be NULL\n";
        }

        if(errorMessage.equals(""))
            return null;

        return errorMessage;
    }

    @Override
    public String toString() {
        return "Mail{" +
                "from='" + from + '\'' +
                ", to='" + to + '\'' +
                ", subject='" + subject + '\'' +
                ", body='" + body + '\'' +
                '}';
    }
}
