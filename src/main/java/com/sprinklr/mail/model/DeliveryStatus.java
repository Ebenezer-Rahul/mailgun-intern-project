package com.sprinklr.mail.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class DeliveryStatus {
    @JsonProperty("tls")
    private boolean tls;
    @JsonProperty("mx-host")
    private String mxHost;
    @JsonProperty("code")
    private long code;
    @JsonProperty("description")
    private String description;
    @JsonProperty("session-seconds")
    private double sessionSeconds;
    @JsonProperty("utf8")
    private boolean utf8;
    @JsonProperty("attempt-no")
    private long attemptNo;
    @JsonProperty("message")
    private String message;
    @JsonProperty("certificate-verified")
    private boolean certificateVerified;

    public boolean getTLS() {
        return tls;
    }

    public void setTLS(boolean value) {
        this.tls = value;
    }

    public String getMXHost() {
        return mxHost;
    }

    public void setMXHost(String value) {
        this.mxHost = value;
    }

    public long getCode() {
        return code;
    }

    public void setCode(long value) {
        this.code = value;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String value) {
        this.description = value;
    }

    public double getSessionSeconds() {
        return sessionSeconds;
    }

    public void setSessionSeconds(double value) {
        this.sessionSeconds = value;
    }

    public boolean getUtf8() {
        return utf8;
    }

    public void setUtf8(boolean value) {
        this.utf8 = value;
    }

    public long getAttemptNo() {
        return attemptNo;
    }

    public void setAttemptNo(long value) {
        this.attemptNo = value;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String value) {
        this.message = value;
    }

    public boolean getCertificateVerified() {
        return certificateVerified;
    }

    public void setCertificateVerified(boolean value) {
        this.certificateVerified = value;
    }

    @Override
    public String toString() {
        return "DeliveryStatus{" +
                "tls=" + tls +
                ", mxHost='" + mxHost + '\'' +
                ", code=" + code +
                ", description='" + description + '\'' +
                ", sessionSeconds=" + sessionSeconds +
                ", utf8=" + utf8 +
                ", attemptNo=" + attemptNo +
                ", message='" + message + '\'' +
                ", certificateVerified=" + certificateVerified +
                '}';
    }
}
