package com.sprinklr.mail.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Arrays;
import java.util.List;

public class EventData {

    @JsonProperty("id")
    private String id;
    @JsonProperty("timestamp")
    private String timestamp;
    @JsonProperty("log-level")
    private String logLevel;
    @JsonProperty("event")
    private String event;
    @JsonProperty("reason")
    private String reason;
    @JsonProperty("severity")
    private String severity;
    @JsonProperty("delivery-status")
    private DeliveryStatus deliveryStatus;
    @JsonProperty("flags")
    private Flags flags;
    @JsonProperty("envelope")
    private Envelope envelope;
    @JsonProperty("message")
    private Message message;
    @JsonProperty("recipient")
    private String recipient;
    @JsonProperty("recipient-domain")
    private String recipientDomain;

    @JsonProperty("ip")
    private String ip;


    @JsonProperty("geo-location")
    private GeoLocation geoLocation;

    @JsonProperty("client-info")
    private ClientInfo clientInfo;
    @JsonProperty("storage")
    private Storage storage;
    @JsonProperty("campaigns")
    private List<Object> campaigns;
    @JsonProperty("tags")
    private List<String> tags;

    @JsonProperty("user-variables")
    private UserVariables userVariables;

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String value) {
        this.timestamp = value;
    }

    public String getLogLevel() {
        return logLevel;
    }

    public void setLogLevel(String value) {
        this.logLevel = value;
    }

    public String getEvent() {
        return event;
    }

    public void setEvent(String value) {
        this.event = value;
    }

    public DeliveryStatus getDeliveryStatus() {
        return deliveryStatus;
    }

    public void setDeliveryStatus(DeliveryStatus value) {
        this.deliveryStatus = value;
    }

    public Flags getFlags() {
        return flags;
    }

    public void setFlags(Flags value) {
        this.flags = value;
    }

    public Envelope getEnvelope() {
        return envelope;
    }

    public void setEnvelope(Envelope value) {
        this.envelope = value;
    }

    public Message getMessage() {
        return message;
    }

    public void setMessage(Message value) {
        this.message = value;
    }

    public String getRecipient() {
        return recipient;
    }

    public void setRecipient(String value) {
        this.recipient = value;
    }

    public String getRecipientDomain() {
        return recipientDomain;
    }

    public void setRecipientDomain(String value) {
        this.recipientDomain = value;
    }

    public String getIp() {
        return ip;
    }
    public void setIp(String value) {
        this.ip = value;
    }

    public Storage getStorage() {
        return storage;
    }

    public void setStorage(Storage value) {
        this.storage = value;
    }

    public List<Object> getCampaigns() {
        return campaigns;
    }

    public void setCampaigns(List<Object> value) {
        this.campaigns = value;
    }

    public List<String> getTags() {
        return tags;
    }

    public void setTags(List<String> value) {
        this.tags = value;
    }

    public UserVariables getUserVariables() {
        return userVariables;
    }

    public void setUserVariables(UserVariables value) {
        this.userVariables = value;
    }

    @Override
    public String toString() {
        return "EventData{" +
                "id='" + id + '\'' +
                ", timestamp='" + timestamp + '\'' +
                ", logLevel='" + logLevel + '\'' +
                ", event='" + event + '\'' +
                ", reason='" + reason + '\'' +
                ", severity='" + severity + '\'' +
                ", deliveryStatus=" + deliveryStatus +
                ", flags=" + flags +
                ", envelope=" + envelope +
                ", message=" + message +
                ", recipient='" + recipient + '\'' +
                ", recipientDomain='" + recipientDomain + '\'' +
                ", ip='" + ip + '\'' +
                ", geoLocation=" + geoLocation +
                ", clientInfo=" + clientInfo +
                ", storage=" + storage +
                ", campaigns=" + campaigns +
                ", tags=" + tags +
                ", userVariables=" + userVariables +
                '}';
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getSeverity() {
        return severity;
    }

    public void setSeverity(String severity) {
        this.severity = severity;
    }

    public GeoLocation getGeoLocation() {
        return geoLocation;
    }

    public void setGeoLocation(GeoLocation geoLocation) {
        this.geoLocation = geoLocation;
    }

    public ClientInfo getClientInfo() {
        return clientInfo;
    }

    public void setClientInfo(ClientInfo clientInfo) {
        this.clientInfo = clientInfo;
    }
}

