package com.sprinklr.mail.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Headers {
    @JsonProperty("to")
    private String to;
    @JsonProperty("message-id")
    private String messageID;
    @JsonProperty("from")
    private String from;
    @JsonProperty("subject")
    private String subject;

    public String getTo() {
        return to;
    }

    public void setTo(String value) {
        this.to = value;
    }

    public String getMessageID() {
        return messageID;
    }

    public void setMessageID(String value) {
        this.messageID = value;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String value) {
        this.from = value;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String value) {
        this.subject = value;
    }

    @Override
    public String toString() {
        return "Headers{" +
                "to='" + to + '\'' +
                ", messageID='" + messageID + '\'' +
                ", from='" + from + '\'' +
                ", subject='" + subject + '\'' +
                '}';
    }
}
