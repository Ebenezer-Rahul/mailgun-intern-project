package com.sprinklr.mail.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ClientInfo {
    @JsonProperty("client-os")
    private String clientOS;

    @JsonProperty("device-type")
    private String deviceType;
    @JsonProperty("client-name")
    private String clientName;

    @JsonProperty("client-type")
    private String clientType;

    @JsonProperty("user-agent")
    private String userAgent;

    public String getClientOS() {
        return clientOS;
    }

    public void setClientOS(String clientOS) {
        this.clientOS = clientOS;
    }

    public String getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(String deviceType) {
        this.deviceType = deviceType;
    }

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    public String getClientType() {
        return clientType;
    }

    public void setClientType(String clientType) {
        this.clientType = clientType;
    }

    public String getUserAgent() {
        return userAgent;
    }

    public void setUserAgent(String userAgent) {
        this.userAgent = userAgent;
    }

    @Override
    public String toString() {
        return "ClientInfo{" +
                "clientOS='" + clientOS + '\'' +
                ", deviceType='" + deviceType + '\'' +
                ", clientName='" + clientName + '\'' +
                ", clientType='" + clientType + '\'' +
                ", userAgent='" + userAgent + '\'' +
                '}';
    }
}
