package com.sprinklr.mail.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Flags {
    @JsonProperty("is-routed")
    private boolean isRouted;
    @JsonProperty("is-authenticated")
    private boolean isAuthenticated;
    @JsonProperty("is-system-test")
    private boolean isSystemTest;
    @JsonProperty("is-test-mode")
    private boolean isTestMode;

    public boolean getIsRouted() {
        return isRouted;
    }

    public void setIsRouted(boolean value) {
        this.isRouted = value;
    }

    public boolean getIsAuthenticated() {
        return isAuthenticated;
    }

    public void setIsAuthenticated(boolean value) {
        this.isAuthenticated = value;
    }

    public boolean getIsSystemTest() {
        return isSystemTest;
    }

    public void setIsSystemTest(boolean value) {
        this.isSystemTest = value;
    }

    public boolean getIsTestMode() {
        return isTestMode;
    }

    public void setIsTestMode(boolean value) {
        this.isTestMode = value;
    }

    @Override
    public String toString() {
        return "Flags{" +
                "isRouted=" + isRouted +
                ", isAuthenticated=" + isAuthenticated +
                ", isSystemTest=" + isSystemTest +
                ", isTestMode=" + isTestMode +
                '}';
    }
}
