package com.sprinklr.mail.response.model;

import com.mailgun.model.message.MessageResponse;

public class MailResponse {
    public String error;

    public String messageID;

    public MailResponse(MessageResponse messageResponse, String error) {
        this.messageID = messageResponse.getId();
        this.error = error;
    }

    public MailResponse(MessageResponse messageResponse) {
        this.messageID = messageResponse.getId();
        this.error = null;
    }

    public MailResponse(String error) {
        this.messageID = null;
        this.error = error;
    }

    public MailResponse(String error, String messageID) {
        this.messageID = messageID;
        this.error = error;
    }

}
