package com.sprinklr.mail.validator;


import com.sprinklr.mail.Application;
import com.sprinklr.mail.model.Signature;
import org.apache.commons.codec.digest.HmacAlgorithms;
import org.apache.commons.codec.digest.HmacUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Logger;

@Component
public class Validator {

    private final String apiSecret = Application.API_SECRET;
    private final Logger logger = Logger.getLogger(Validator.class.toString());

    @Autowired
    private HmacUtils hm256;
    public boolean validate(Signature signature) {
        String timestamp = signature.getTimestamp();
        String token = signature.getToken();
        String uniqueSignature = timestamp + token;
        //hm256 object can be used again and again
        String generatedSignature = hm256.hmacHex(uniqueSignature);
        logger.info(generatedSignature + ", "+ signature.getSignature());
        return generatedSignature.equals(signature.getSignature());
    }


}
