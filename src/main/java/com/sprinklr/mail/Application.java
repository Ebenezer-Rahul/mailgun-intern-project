package com.sprinklr.mail;

import com.mailgun.api.v3.MailgunMessagesApi;
import com.mailgun.client.MailgunClient;
import org.apache.commons.codec.digest.HmacAlgorithms;
import org.apache.commons.codec.digest.HmacUtils;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;

import java.util.Date;
import java.util.logging.Logger;

@SpringBootApplication
@Import(com.sprinklr.mail.database.DataBaseConfig.class)
public class Application {

	private final String APIKEY = "356d0cf6d3738a569542c110cc132ca3-af778b4b-921f45d9";

	public static final String API_SECRET  = "356d0cf6d3738a569542c110cc132ca3-af778b4b-921f45d9";
	public Logger logger = Logger.getLogger(Application.class.toString());

	public static void main(String[] args) {
		ApplicationContext ctx =  SpringApplication.run(Application.class, args);
	}

	@Bean
	public HmacUtils getHmacUtils() {
		return new HmacUtils(HmacAlgorithms.HMAC_SHA_256, API_SECRET);
	}

	@Bean
	public MailgunMessagesApi myMessageApi() {
		return MailgunClient.config(APIKEY).createApi(MailgunMessagesApi.class);
	}

}
