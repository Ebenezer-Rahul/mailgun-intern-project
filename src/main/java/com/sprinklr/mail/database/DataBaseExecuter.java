package com.sprinklr.mail.database;

import com.mongodb.client.MongoCollection;
import com.sprinklr.mail.model.Event;
import com.sprinklr.mail.model.Message;
import com.sprinklr.mail.model.Signature;
import org.bson.Document;


public interface DataBaseExecuter {
    public void execute(MongoCollection<Event> collection);
}
