package com.sprinklr.mail.database;

import com.mongodb.client.MongoCursor;
import com.sprinklr.mail.model.Event;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.lang.annotation.ElementType;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import static com.mongodb.client.model.Filters.eq;

@Component
public class Dao {
    @Autowired
    DataBase database;

    private final Logger logger = Logger.getLogger(Dao.class.toString());
    public void insert(Event event) {

        database.execute("Events",
                (collection) -> {
                    collection.insertOne(event);
                }
                );
    }

    public List<Event> getEventsOfMessage(String messageId) {
        List<Event> events= new ArrayList<>();
        database.execute("Events", (collection) -> {
            MongoCursor<Event> cursor = collection
                    .find(eq("eventData.message.headers.messageID", messageId))
                    .iterator();

            while(cursor.hasNext()){
                events.add(cursor.next());
            }
            cursor.close();

        });
        return events;
    }

    public List<Event> getFailedEvents() {
        List<Event> events= new ArrayList<>();
        database.execute("Events", (collection) -> {
            MongoCursor<Event> cursor = collection
                    .find(eq("eventData.event", "failed"))
                    .iterator();

            while(cursor.hasNext()){
                events.add(cursor.next());
            }
            cursor.close();
        });
        return events;
    }
}
