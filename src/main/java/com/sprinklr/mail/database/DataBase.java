package com.sprinklr.mail.database;

import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.sprinklr.mail.model.Event;
import com.sprinklr.mail.model.Message;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.logging.Logger;

@Component
public class DataBase {

    @Autowired
    private MongoDatabase database;

    private final Logger logger = Logger.getLogger(DataBase.class.toString());

    public void execute(String collectionName,DataBaseExecuter exec) {
        if (collectionName.equals("Events")) {
            logger.info("Executing Database Command on EventsCollection");
            MongoCollection<Event> collection = database.getCollection("Events", Event.class);
            exec.execute(collection);
            logger.info("Executed Database Command on EventsCollection");
        }
    }


}
