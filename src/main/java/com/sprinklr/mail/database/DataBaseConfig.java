package com.sprinklr.mail.database;


import com.mongodb.*;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoDatabase;
import org.bson.codecs.configuration.CodecRegistry;
import org.bson.codecs.pojo.PojoCodecProvider;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.logging.Logger;

import static org.bson.codecs.configuration.CodecRegistries.fromProviders;
import static org.bson.codecs.configuration.CodecRegistries.fromRegistries;


@Configuration
public class DataBaseConfig {

    private static final Logger logger = Logger.getLogger(DataBaseConfig.class.toString());

    private static final String databaseName = "MailTracker";
    public final String url = "mongodb+srv://guest:mypassword@cluster0.u7ti0zd.mongodb.net/?retryWrites=true&w=majority";

    @Bean
    public ConnectionString connectionString() {
        logger.info("creating Mongo connection string");
        return new ConnectionString(url);
    }
    @Bean
    public ServerApi serverApi() {
        logger.info("creating Mongo ServerAPI");

        return ServerApi.builder()
                .version(ServerApiVersion.V1)
                .build();
    }

    @Bean
    public MongoClientSettings mongoClientSettings() {
        logger.info("creating MongoClient Settings");
        CodecRegistry pojoCodecRegistry = fromProviders(
                    PojoCodecProvider
                            .builder()
                            .register("com.sprinklr.mail.model")
                            .register(Object.class)
                            .build()
                    );
        CodecRegistry codecRegistry = fromRegistries(
                MongoClientSettings.getDefaultCodecRegistry(),
                pojoCodecRegistry);
        return MongoClientSettings.builder()
                        .applyConnectionString(connectionString())
                        .codecRegistry(codecRegistry)
                        .serverApi(serverApi()).build();
    }

    @Bean
    public MongoClient mongoClient() {

        try {
            logger.info("creating MongoClients");
            MongoClient mongoClient = MongoClients.create(mongoClientSettings());
            logger.info("created MongoClients");
            return mongoClient;
        }

        catch (MongoClientException e) {
            logger.warning("Error Creating MongoClient");
            throw new RuntimeException(e);
        }
    }

    @Bean
    public MongoDatabase database() {
        return mongoClient().getDatabase(databaseName);
    }
}
