package com.sprinklr.mail.mailgunapi;

import com.mailgun.api.v3.MailgunMessagesApi;
import com.mailgun.model.message.Message;
import com.mailgun.model.message.MessageResponse;
import com.sprinklr.mail.model.Mail;
import com.sprinklr.mail.response.model.MailResponse;
import org.apache.coyote.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import java.io.*;
import java.net.InetAddress;
import java.util.Properties;
import java.util.Date;
import javax.mail.*;
import javax.mail.internet.*;
import com.sun.mail.smtp.*;
import java.util.logging.Logger;

@Component
public class MessagesAPI {

    private MailgunMessagesApi mailgunMessagesApi;
    private Logger logger = Logger.getLogger(MessagesAPI.class.toString());

    @Value("sandbox4e18f5a7c35b424aa58a59a53306d3b8.mailgun.org")
    private String domain;

    @Value("postmaster@sandbox4e18f5a7c35b424aa58a59a53306d3b8.mailgun.org")
    private String smtpUserName;

    @Value("5f00fb59a0c9eda79731d3f668eb1734-af778b4b-bc6eed91")
    private String smtpPassword;

    @Autowired
    MessagesAPI(MailgunMessagesApi mailgunMessagesApi) {
        this.mailgunMessagesApi = mailgunMessagesApi;
    }

    public MailResponse sendMessage(Message message) {
        logger.info("Sending message");
        try {
            MessageResponse messageResponse = mailgunMessagesApi.sendMessage(domain, message);
            logger.info("Message sent" + messageResponse);
            return new MailResponse(messageResponse);
        } catch (Exception e) {
            logger.info("Exception Raised: " +e);
            return new MailResponse("Some Exception was Raised");
        }
    }

    public MailResponse sendMessageSMTP(Mail mail, boolean isHTML) {
        String messageID = null;
        Properties props = System.getProperties();
        props.put("mail.smtp.host", "smtp.mailgun.org");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.port", 587);

        try{
            Session session = Session.getInstance(props, null);
            MimeMessage msg = new MimeMessage(session);

            msg.setFrom(new InternetAddress(mail.getFrom()));

            InternetAddress[] addrs = InternetAddress.parse(mail.getTo(), false);
            msg.setRecipients(MimeMessage.RecipientType.TO, addrs);

            msg.setSubject(mail.getSubject());
            if(isHTML) {
                msg.setContent(mail.getBody(), "text/html");
            }
            else
            {
                msg.setText(mail.getBody());
            }
            msg.setSentDate(new Date());

            SMTPTransport t =
                    (SMTPTransport) session.getTransport("smtp");
            logger.info("Sending the mail");
            t.connect("smtp.mailgun.org",smtpUserName,smtpPassword);
            t.sendMessage(msg, msg.getAllRecipients());
            logger.info("message Id : " + msg.getMessageID());
            messageID = msg.getMessageID();
            logger.info("Response: " + t.getLastServerResponse());

            t.close();

            return new MailResponse((String)null, messageID);
        }
        catch(Exception e) {
            logger.info("Error :" + e);
            return new MailResponse(e.toString());
        }
    }
}
