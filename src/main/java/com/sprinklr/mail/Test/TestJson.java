package com.sprinklr.mail.Test;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.logging.Logger;

public class TestJson {

    Logger logger = Logger.getLogger(TestJson.class.toString());
    @JsonProperty("bob")
    public User userA;
    @JsonProperty("alice")
    public User userB;

    public void print(){
        logger.info("Test Json");
        logger.info("UserA :" + userA);
        logger.info("UserB :" + userB);
    }
}

class User {
    @JsonProperty("user-name")
    public String name;

    @Override
    public String toString() {
        return "user-name: " + name;
    }
}