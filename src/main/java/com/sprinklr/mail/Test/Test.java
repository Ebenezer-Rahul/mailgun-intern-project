package com.sprinklr.mail.Test;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.json.JsonMapper;
import com.mailgun.model.message.MessageResponse;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.logging.Logger;

@RestController
public class Test {

    @Autowired
    private MessageTrackTest messageTrackTest;

    private final Logger logger = Logger.getLogger(Test.class.toString());

    @GetMapping("/sendMessage")
    public String events(HttpServletRequest request) {
        MessageResponse messageResponse = messageTrackTest.sendMessage();
        logger.info("Message sent: " + messageResponse);
        return "Track your Message: " + messageResponse.getId();
    }


    @PostMapping("/JsonTest")
    public String jsonToObj(@RequestBody String request) {
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            TestJson test = objectMapper.readValue(request, TestJson.class);
            test.print();
            return test.toString();
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

}
