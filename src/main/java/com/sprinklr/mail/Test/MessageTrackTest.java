package com.sprinklr.mail.Test;

import com.mailgun.api.v3.MailgunMessagesApi;
import com.mailgun.model.message.MessageResponse;
import com.mailgun.model.message.Message;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class MessageTrackTest {
    private MailgunMessagesApi mailgunMessagesApi;

    @Value("sandbox4e18f5a7c35b424aa58a59a53306d3b8.mailgun.org")
    private String domain;

    @Autowired
    MessageTrackTest(MailgunMessagesApi mailgunMessagesApi){
        this.mailgunMessagesApi = mailgunMessagesApi;
    }

    public MessageResponse sendMessage() {
        Message message = Message.builder()
                .from("rahul@sandbox4e18f5a7c35b424aa58a59a53306d3b8.mailgun.org")
                .to("vangur_b200818cs@nitc.ac.in")
                .subject("test message")
                .html("<h1>This is a Test Message Sent Form Mail Gun Please Ignore it</h1><a href='www.sprinklr.com'>click Here</a>")
                .tracking(true)
                .build();
        return mailgunMessagesApi.sendMessage(domain,message);
    }

}
