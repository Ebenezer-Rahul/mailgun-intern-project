package com.sprinklr.mail.tacker;


import com.mailgun.model.message.Message;
import com.mailgun.model.message.MessageResponse;
import com.sprinklr.mail.database.Dao;
import com.sprinklr.mail.mailgunapi.MessagesAPI;
import com.sprinklr.mail.model.Event;
import com.sprinklr.mail.model.Mail;
import com.sprinklr.mail.response.model.MailResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.logging.Logger;

@RestController
public class API {

    public static final Logger logger = Logger.getLogger(API.class.toString());

    @Autowired
    MessagesAPI messagesAPI;

    @Autowired
    Dao dao;

    @PostMapping("/mailgun-api/sendMessage")
    public MailResponse apiSendMessage(@RequestBody Mail mail) {
        String errorMessage = mail.checkNULL();
        if(errorMessage != null) {
            logger.info("error :" + errorMessage);
            return new MailResponse(errorMessage);
        }
        Message message = Message
                .builder()
                .from(mail.getFrom())
                .to(mail.getTo())
                .subject(mail.getSubject())
                .text(mail.getBody())
                .tracking(true)
                .build();

        MailResponse mailResponse =  messagesAPI.sendMessage(message);
        return mailResponse;
    }

    @PostMapping("/mailgun-api/html/sendMessage")
    public MailResponse apiSendHTMLMessage(@RequestBody Mail mail) {
        String errorMessage = mail.checkNULL();
        if(errorMessage != null) {
            logger.info("error :" + errorMessage);
            return new MailResponse(errorMessage);
        }
        Message message = Message
                .builder()
                .from(mail.getFrom())
                .to(mail.getTo())
                .subject(mail.getSubject())
                .html(mail.getBody())
                .tracking(true)
                .build();

        MailResponse mailResponse =  messagesAPI.sendMessage(message);
        return mailResponse;
    }

    @PostMapping("/smtp/sendMessage")
    public MailResponse smtpSendMessage(@RequestBody Mail mail) {
        String errorMessage = mail.checkNULL();
        logger.info("error :" + errorMessage);
        if(errorMessage != null) {
            logger.info("error :" + errorMessage);
            return new MailResponse(errorMessage);
        }
        return messagesAPI.sendMessageSMTP(mail, false);
    }

    @PostMapping("/smtp/html/sendMessage")
    public MailResponse smtpSendHTMLMessage(@RequestBody Mail mail) {
        String errorMessage = mail.checkNULL();
        logger.info("error :" + errorMessage);
        if(errorMessage != null) {
            logger.info("error :" + errorMessage);
            return new MailResponse(errorMessage);
        }
        return messagesAPI.sendMessageSMTP(mail, true);
    }

    @GetMapping("/track/message={message_id}")
    public List<Event> TrackMessage(@PathVariable String message_id) {
        logger.info("Getting All Events On Message");
        return dao.getEventsOfMessage(message_id);
    }

    @GetMapping("/track/failedMessages")
    public List<Event> TrackFailedMessages() {
        return dao.getFailedEvents();
    }
}
