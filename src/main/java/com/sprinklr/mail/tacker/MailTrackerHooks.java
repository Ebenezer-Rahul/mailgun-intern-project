package com.sprinklr.mail.tacker;


import com.sprinklr.mail.database.Dao;
import com.sprinklr.mail.model.Event;

import java.util.List;
import java.util.logging.Logger;

import com.sprinklr.mail.validator.Validator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
class MailTrackerHooks {

    private static final Logger logger = Logger.getLogger(MailTrackerHooks.class.toString());
    @Autowired
    Dao dao;

    @Autowired
    Validator validator;

    @PostMapping("/events/delivered")
    public String handleDelivery(@RequestBody Event event) {
        if(! validator.validate(event.getSignature())) {
            logger.info("Signature validation failed");
            return "False Event";
        }
        dao.insert(event);
        logger.info("Timestamp of delivery :" + event.getSignature().getTimestamp());
        logger.info(event.toString());
        logger.info("Your Email is delivered");
        return "Your Email is Delivered";
    }

    @PostMapping("/events/opened")
    public String handleOpen(@RequestBody Event event) {
        if(! validator.validate(event.getSignature())) {
            logger.info("Signature validation failed");
            return "False Event";
        }
        dao.insert(event);
        logger.info("Timestamp when Opened :" + event.getSignature().getTimestamp());
        logger.info(event.toString());
        logger.info("Your Email is Opened");
        return "Your Email is Opened";
    }

    @PostMapping("/events/failure/temporary")
    public String handleTemporaryFailure(@RequestBody Event event) {
        if(! validator.validate(event.getSignature())) {
            logger.info("Signature validation failed");
            return "False Event";
        }
        dao.insert(event);
        logger.info("Timestamp when Failed :" + event.getSignature().getTimestamp());
        logger.info(event.toString());
        logger.info("Your Email delivery Failed");
        return "Your Email delivery Failed";
    }

    @PostMapping("/events/failure/permanent")
    public String handlePermanentFailure(@RequestBody Event event) {
        if(! validator.validate(event.getSignature())) {
            logger.info("Signature validation failed");
            return "False Event";
        }
        dao.insert(event);
        logger.info("Email delivery Failed Permanently at :" + event.getSignature().getTimestamp());
        logger.info(event.toString());
        return "Your Email Failed Permanently";
    }

    @PostMapping("/events/reported-spam")
    public String handleReportedSpam(@RequestBody Event event) {
        if(! validator.validate(event.getSignature())) {
            logger.info("Signature validation failed");
            return "False Event";
        }
        dao.insert(event);
        logger.info("Your Email is reported as Spam at" + event.getSignature().getTimestamp());
        logger.info(event.toString());
        return "Your Email reported as Spam";
    }

    @PostMapping("/events/clicked")
    public String handleClick(@RequestBody Event event) {
        if(! validator.validate(event.getSignature())) {
            logger.info("Signature validation failed");
            return "False Event";
        }
        dao.insert(event);
        logger.info("Your Link in the Email is clicked" + event.getSignature().getTimestamp());
        logger.info(event.toString());
        return "Your Link in the Email is clicked" ;
    }

    @PostMapping("/events/unsubscribe")
    public String handleUnsubscribe(@RequestBody Event event) {
        if(! validator.validate(event.getSignature())) {
            logger.info("Signature validation failed");
            return "False Event";
        }
        dao.insert(event);
        logger.info("Your Link in the Email is clicked" + event.getSignature().getTimestamp());
        logger.info(event.toString());
        return "You have unsubscribed";
    }





}
